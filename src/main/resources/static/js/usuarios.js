window.addEventListener('DOMContentLoaded', event => {
    // Simple-DataTables
    // https://github.com/fiduswriter/Simple-DataTables/wiki

    const datatablesSimple = document.getElementById('datatablesSimple');
    cargarusuarios();
    if (datatablesSimple) {
        new simpleDatatables.DataTable(datatablesSimple);
    }
});
async function cargarusuarios() {

    const request = await fetch('api/usuarios', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    });
    const usuarios = await request.json();


    let listadoHtml = '';

    for (let usuario of usuarios) {
        let botoneliminar = 'a href="#" onclick="eliminarusuario(' + usuario.idusuario + ')" class="btn btn-danger btn-circle btn-sm"><i class="fas fa-trash"></i></a>'
        let usuarioHtml = '<tr>< td >' + usuario.idusuario + '</td><td>' + usuario.nombre
            + '</td><td>' + usuario.apellido + '</td><td>' + usuario.origen + '</td><td>'
            + usuario.destino + '</td><td>' + usuario.email
            + '</td><td><</td>' + botoneliminar + '</tr > ';

        listadoHtml += usuarioHtml;
    }

    console.log(usuarios);


    document.querySelector('#usuarios tbody').outerHTML = listadoHtml;
}
async function eliminarusuario(idusuario) {

    if(!confirm('¿Desea eliminar este usuario?')){
        return;
    }

    const request = await fetch('api/usuarios' + idusuario, {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    });

    location.reload()

}