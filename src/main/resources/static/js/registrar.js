window.addEventListener('DOMContentLoaded', event => {
    // Simple-DataTables
    // https://github.com/fiduswriter/Simple-DataTables/wiki

    //const datatablesSimple = document.getElementById('datatablesSimple');
    //registrarusuarios();
    //if (datatablesSimple) {
    //    new simpleDatatables.DataTable(datatablesSimple);
    //}
});

async function registrarusuarios() {
    let datos ={};
    datos.nombre= document.getElementById('inputFirstName').value;
    datos.apellido= document.getElementById('inputLastName').value;
    datos.email= document.getElementById('inputEmail').value;
    datos.password= document.getElementById('inputPassword').value;

    let repetirPassword = document.getElementById('inputPasswordConfirm').value;

    if (repetirPassword != datos.password){
        alert('La contraseña no igual');
        return;
    }
    
    const request = await fetch('api/usuarios', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(datos)
    });

}