window.addEventListener('DOMContentLoaded', event => {
    // Simple-DataTables
    // https://github.com/fiduswriter/Simple-DataTables/wiki

    //const datatablesSimple = document.getElementById('datatablesSimple');
    //registrarusuarios();
    //if (datatablesSimple) {
    //    new simpleDatatables.DataTable(datatablesSimple);
    //}
});

async function iniciarsesion() {
    let datos ={};
    datos.email= document.getElementById('inputEmail').value;
    datos.password= document.getElementById('inputPassword').value;
    datos.password= document.getElementById('inputRememberPassword').value;

    const request = await fetch('api/login', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(datos)
    });
    const respuesta = await request.json();
    if (respuesta == 'OK'){
        window.location.href ="usuarios.html"
    }else {
        alert("Las credenciales son incorrectas. Intente nuevamente");
    }

}