package com.mintic.vuelafacil;

import com.mintic.vuelafacil.vuelafacilapp;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class vuelafacilapp {

	public static void main(String[] args) {
		SpringApplication.run(vuelafacilapp.class, args);
	}
}