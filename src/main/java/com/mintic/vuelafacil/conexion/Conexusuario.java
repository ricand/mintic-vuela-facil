package com.mintic.vuelafacil.conexion;

import java.util.List;
import com.mintic.vuelafacil.model.Usuario;

public interface Conexusuario {
    List<Usuario> getUsuarios();

    void eliminar(int idusuario);

    void registrar(Usuario usuario);

    boolean verificarCredenciales(Usuario usuario);

}
