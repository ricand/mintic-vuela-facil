package com.mintic.vuelafacil.conexion;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import com.mintic.vuelafacil.model.Usuario;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class Impconexion implements Conexusuario {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    @Transactional
    public List<Usuario> getUsuarios() {
        String query = "FROM Usuario";
        return entityManager.createQuery(query).getResultList();
    }

    @Override
    public void eliminar(int idusuario) {
        Usuario usuario = entityManager.find(Usuario.class, idusuario);
        entityManager.remove(usuario);

    }

    @Override
    public void registrar(Usuario usuario) {
        entityManager.merge(usuario);
    }

    @Override
    public boolean verificarCredenciales(Usuario usuario) {
        String query = "FROM Usuario WHERE email = :email AND password = :password ";
        List<Usuario> lista = entityManager.createQuery(query).getResultList();
        //.setParameter("email", email)
        //.setParameter("password", password)
        //.getResultList();
        return !lista.isEmpty();
    }

}