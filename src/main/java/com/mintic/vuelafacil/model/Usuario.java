package com.mintic.vuelafacil.model;

//import javax.annotation.processing.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jdbc.core.JdbcTemplate;
//import java.sql.SQLException;

//import lombok.Data;

/**
 * Con Entity indico que es una tabla dentro de Sql
 */

@Entity
@Table(name = "usuarios")
public class Usuario {

    //@Autowired
    //JdbcTemplate jdbcTemplate;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idusuario")
    private int idusuario;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "apellido")
    private String apellido;
    @Column(name = "origen")
    private String origen;
    @Column(name = "destino")
    private String destino;
    @Column(name = "email")
    private String email;
    public String getPassword;

    public int getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(int idusuario) {
        this.idusuario = idusuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    // public void registrarUsuario() throws ClassNotFoundException, SQLException {
    //     String sql = "INSERT INTO usuario(idusuario, nombre, apellido, origen, destino, email) VALUES(?,?,?,?,?,?);";
    //     jdbcTemplate.execute(sql);
    // }

    @Override
    public String toString() {
        return "Usuario [apellido=" + apellido + ", destino=" + destino + ", email=" + email + 
        ", idusuario=" + idusuario + ", nombre=" + nombre + ", origen=" + origen + "]";
    }

}
