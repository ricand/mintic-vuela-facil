package com.mintic.vuelafacil.cotroladores;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.mintic.vuelafacil.conexion.Conexusuario;
import com.mintic.vuelafacil.model.Usuario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Usuariocontrolador {

    @Autowired
    private Conexusuario conexusuario;

    @RequestMapping(value = "api/usuarios/{idusuario}", method = RequestMethod.GET)
    public Usuario getUsuario(@PathVariable int idusuario) {
        Usuario usuario = new Usuario();
        usuario.setIdusuario(idusuario);
        usuario.setNombre("Javier");
        usuario.setApellido("Garzon");
        usuario.setOrigen("Bogota");
        usuario.setDestino("Medellin");
        usuario.setEmail("javier@gmail.com");
        return usuario;

    }

    @RequestMapping(value = "api/usuarios", method = RequestMethod.GET)
    public List<Usuario> getUsuarios() {
        return Conexusuario.getUsuarios();
    }

    @RequestMapping(value = "api/usuarios", method = RequestMethod.POST)
    public void registrarUsuarios(@RequestBody Usuario usuario) {
        Conexusuario.registrar(usuario);
    }

    @RequestMapping(value = "api/usuarios/{idusuario}", method = RequestMethod.DELETE)
    public void eliminar(@PathVariable int idusuario) {
        conexusuario.eliminar(idusuario);
    }

    @RequestMapping(value = "api/login", method = RequestMethod.POST)
    public String login(@RequestBody Usuario usuario) {
        if (Conexusuario.verificarCredenciales(usuario)) {
            return "OK";
        }
        return "FAIL";
    }

}
